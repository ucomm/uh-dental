# UH-Dental Font

Here is an icomoon-produced JSON file that represents the entire UH-Dental icon font.

This JSON file is necessary for the user who is updating the icon font with additional icons/change icons/etc.